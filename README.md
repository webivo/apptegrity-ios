# About

> Apptegrity iOS SDK.

## Release Notes

If you are upgrading from a previous version of the Apptegrity iOS SDK, make sure you read our release notes guide.

## Quick Start

### Requirements

- iOS 13.0 or later

### Installation

#### CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. For usage and installation instructions, visit their website. To integrate Apptegrity into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
pod 'Apptegrity' , '~> 1.0.1'
```

#### Carthage

[Carthage](https://github.com/Carthage/Carthage) is a decentralized dependency manager that builds your dependencies and provides you with binary frameworks. To integrate Apptegrity into your Xcode project using Carthage, specify it in your `Cartfile`:

```ogdl
git "https://IordacheClaudiu@bitbucket.org/webivo/apptegrity-ios.git" ~> 1.0.1
```

Then run `carthage update`
If this is your first time using Carthage in the project, you'll need to go through some additional steps as explained [over at Carthage](https://github.com/Carthage/Carthage#adding-frameworks-to-an-application).

### Usage

1. Import the Firebase module in your `UIApplicationDelegate`:
```swift
import Apptegrity
```

2. Configure a Apptegrity shared instance, typically in your app's `application:didFinishLaunchingWithOptions: method:`
```swift
Apptegrity.initialize(with: API_KEY)
```
<p class="tip">The <code>API_KEY</code> can be found: My Account -> Api Key</p>

3. After you add the initialization code, you can use the built-in screen.
```swift
/// Presents an UIViewController embedded in an UINavigationViewController.
/// Details screen will be pushed.
let viewController: UIViewController = ...
Apptegrity.present(from: viewController)
```