//
//  ViewController.swift
//  ApptegrityExample
//
//  Created by Claudiu Iordache on 15/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import UIKit
import CoreLocation
import Apptegrity

class ViewController: UIViewController {
    
    private var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Apptegrity.initialize(with: "9AH37SX-JF54BKW-GCECJCC-ZQ5MWXH")
//        Apptegrity.initialize(with: "2MS78GG-6NW4016-J9R0VSP-FZKS6JD")
        self.locationManager = CLLocationManager()
        self.locationManager.requestAlwaysAuthorization()
    }
    
    @IBAction func showWhatsNew(_ sender: Any) {
        Apptegrity.present(from: self)
    }
}

