//
//  CarrierInfo.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 18/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import CoreTelephony

struct CarrierInfo: Codable {
    
    let carrierName: String?
    let mobileCountryCode: String?
    let mobileNetworkCode: String?
    let isoCountryCode: String?
    
    init(carrier: CTCarrier) {
        self.carrierName = carrier.carrierName
        self.mobileCountryCode = carrier.mobileCountryCode
        self.mobileNetworkCode = carrier.mobileNetworkCode
        self.isoCountryCode = carrier.isoCountryCode
    }
}
