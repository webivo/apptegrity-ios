//
//  Feature.swift
//  Weback
//
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
import UIKit

struct State: Codable {
    let label: String
    let value: String
}

struct Feature: Codable, Hashable {
    let id: String
    let title: String
    let state: State
    let content: String
    let imageUrl: String?
    let totalFeedback: Int?
    let totalClicks: Int?
    let totalViews: Int?
    let timestamp: Timestamp
    
    var date: Date {
        return Date(timeIntervalSince1970: TimeInterval(timestamp.seconds))
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: Feature, rhs: Feature) -> Bool {
        return lhs.id == rhs.id &&
            lhs.title == rhs.title &&
            lhs.content == rhs.content &&
            lhs.imageUrl == rhs.imageUrl &&
            lhs.timestamp == rhs.timestamp
    }
}

extension Feature {
    var stateColor: UIColor {
        switch state.value.lowercased() {
        case "fix":
            return ColorName.fix.color
        case "improvement":
            return ColorName.improvement.color
        case "coming soon":
            return ColorName.comingSoon.color
        case "announcement":
            return ColorName.announcement.color
        case "new":
            return ColorName.new.color
        default:
            return .black
        }
    }
}

struct Timestamp: Codable {
    let seconds: Int
    let nanoseconds: Int
}

extension Timestamp: Equatable {
    static func == (lhs: Timestamp, rhs: Timestamp) -> Bool {
        return lhs.seconds == rhs.seconds &&
            lhs.nanoseconds == rhs.nanoseconds
    }
}

private extension Timestamp {
    enum CodingKeys: String, CodingKey {
        case seconds = "_seconds"
        case nanoseconds = "_nanoseconds"
    }
}
