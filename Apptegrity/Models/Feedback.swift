//
//  Feedback.swift
//  Weback
//
//  Created by Claudiu Iordache on 02/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

struct Feedback: Codable {
    let text: String
}
