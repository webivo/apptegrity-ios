//
//  Apptegrity.h
//  Apptegrity
//
//  Created by Claudiu Iordache on 30/11/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Apptegrity.
FOUNDATION_EXPORT double ApptegrityVersionNumber;

//! Project version string for Apptegrity.
FOUNDATION_EXPORT const unsigned char ApptegrityVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Apptegrity/PublicHeader.h>


