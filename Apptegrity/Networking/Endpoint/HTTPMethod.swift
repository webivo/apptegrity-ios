//
//  HTTPMethod.swift
//  Weback
//
//  Created by Claudiu Iordache on 30/11/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

typealias HTTPHeaders = [String: Any]

extension HTTPHeaders {
    struct Keys {
        static let contentType = "Content-Type"
        static let accept = "Accept"
        static let authorization = "Authorization"
        static let visitorID = "VisitorID"
        static let userAgent = "User-Agent"
    }
    
    struct Values {
        static let applicationJSON = "application/json"
        static let applicationFormEncoded = "application/x-www-form-urlencoded"
    }
}

typealias HTTPParameters = [String: Any]

enum HTTPMethod: String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
    case PATCH = "PATCH"
    case HEAD = "HEAD"
}
