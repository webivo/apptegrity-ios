//
//  File.swift
//  Weback
//
//  Created by Claudiu Iordache on 30/11/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

protocol EndpointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var httpTask: HTTPTask { get }
    var requiresAuthentication: Bool { get }
}

extension EndpointType {
//    var baseURL: URL { return URL(string: "https://api-dev-apptegrity.firebaseapp.com")! }
    var baseURL: URL { return URL(string: "https://api.apptegrity.io")! }
    var requiresAuthentication: Bool { return true }
}
