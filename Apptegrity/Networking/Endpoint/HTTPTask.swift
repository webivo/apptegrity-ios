//
//  HTTPTask.swift
//  Weback
//
//  Created by Claudiu Iordache on 30/11/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

enum HTTPRequestDecoration {
    case headers(_ headers: HTTPHeaders)
    case urlParameters(_ urlParameters: HTTPParameters)
    case jsonParameters(_ bodyParameters: HTTPParameters)
    case json(body: Data)
}

struct HTTPTask {
    
    let decorations: [HTTPRequestDecoration]
    
    static func empty() -> HTTPTask {
        return HTTPTask(decorations: [])
    }
}
