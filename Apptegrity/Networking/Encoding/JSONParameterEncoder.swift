//
//  JSONParameterEncoder.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
import os.log

class JSONParameterEncoder: ParameterEncoder {
    func encode(request: inout URLRequest, with parameters: HTTPParameters) {
        do {
            let data = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            request.httpBody = data
        } catch {
            os_log("%{PUBLIC}@", log: OSLog.jsonParameterEncoder, type: .error, error.localizedDescription)
        }
    }
}
