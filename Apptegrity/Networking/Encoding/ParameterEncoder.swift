//
//  ParameterEncoder.swift
//  Weback
//
//  Created by Claudiu Iordache on 30/11/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

protocol ParameterEncoder {
    func encode(request: inout URLRequest, with parameters: HTTPParameters)
}
