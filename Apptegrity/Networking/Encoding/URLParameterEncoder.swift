//
//  URLParameterEncoder.swift
//  Weback
//
//  Created by Claudiu Iordache on 30/11/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

class URLParameterEncoder: ParameterEncoder {
    func encode(request: inout URLRequest, with parameters: HTTPParameters) {
        guard let url = request.url else { return }
        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false),
            !parameters.isEmpty {
            let queryParameters = parameters.map { parameter -> URLQueryItem in
                let value = "\(parameter.value)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                return URLQueryItem(name: parameter.key, value: value)
            }
            urlComponents.queryItems = queryParameters
            request.url = urlComponents.url;
        }
    }
}
