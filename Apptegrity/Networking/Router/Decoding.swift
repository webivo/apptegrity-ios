//
//  Decoding.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 13/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation

protocol Decoding {
    static func decode(data: Data?) throws -> Self?
}

// MARK: - Decodable
extension Array: Decoding where Element : Decodable {}
extension Decoding where Self: Decodable {
    static func decode(data: Data?) throws -> Self? {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return try decoder.decode(Self.self, from: data!)
    }
}

// MARK: - Empty
/// Placeholder for Void response
struct Empty: Decoding {
    static func decode(data: Data?) throws -> Empty? {
        return Empty()
    }
}

// MARK: - Data
/// We want to return normal data
extension Data: Decoding {
    static func decode(data: Data?) throws -> Self? {
        return data
    }
}
