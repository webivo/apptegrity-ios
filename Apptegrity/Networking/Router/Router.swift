//
//  File.swift
//  Weback
//
//  Created by Claudiu Iordache on 30/11/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

class Router {
    
    // MARK: Properties
    private let credentialsStorage: CredentialsStorageProtocol
    private let locationService: LocationService
    private let requestBuilder: RequestBuilder
    private let responseParser: ResponseParser
    
    private lazy var urlSession: URLSession = {
        var configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 40
        return URLSession(configuration: configuration)
    }()
    
    private var queue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "RouterQueue"
        queue.maxConcurrentOperationCount = 5
        return queue
    }()
    
    // MARK: Init
    init(credentialsStorage: CredentialsStorageProtocol,
         locationService: LocationService,
         userAgentService: UserAgentServiceProtocol) {
        self.credentialsStorage = credentialsStorage
        self.locationService = locationService
        self.requestBuilder = HTTPRequestBuilder(credentialsStorage: self.credentialsStorage,
                                                 locationService: self.locationService,
                                                 userAgentService: userAgentService)
        self.responseParser = HTTPResponseParser()
    }
    
    // MARK: Public
    func execute<U: Decoding>(_ endpoint: EndpointType, completion: @escaping ((Result<U, Error>) -> Void)) {
        let networkOperation = DecodableNetworkOperation<U>(urlSession: urlSession,
                                                            endpoint: endpoint,
                                                            requestBuilder: requestBuilder,
                                                            responseParser: responseParser)
        networkOperation.completionHandler = { result in
            completion(result)
        }
        queue.addOperation(networkOperation)
    }
    
    func execute<U: Decoding>(_ endpoint: EndpointType) -> AnyPublisher<U, Error> {
        return Future<U, Error> { promise in
            let networkOperation = DecodableNetworkOperation<U>(urlSession: self.urlSession,
                                                                endpoint: endpoint,
                                                                requestBuilder: self.requestBuilder,
                                                                responseParser: self.responseParser)
            networkOperation.completionHandler = { result in
                promise(result)
            }
            self.queue.addOperation(networkOperation)
        }.eraseToAnyPublisher()
    }
    
    func cancelAll() {
        queue.cancelAllOperations()
    }
}


extension Router {
    
    func execute(_ url: URL) -> AnyPublisher<Data?, Error> {
        var task: URLSessionDataTask?
        return Future<Data?, Error> { [urlSession, responseParser] promise in
            task = urlSession.dataTask(with: url) { (data, response, error) in
                let result = responseParser.parseResponse(data, response: response, error: error)
                promise(result)
            }
            task?.resume()
        }.handleEvents(receiveCancel: { task?.cancel() } )
            .eraseToAnyPublisher()
    }
}
