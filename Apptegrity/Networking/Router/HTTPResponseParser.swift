//
//  HTTPResponseParser.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

protocol ResponseParser {
    func parseResponse(_ data: Data?, response: URLResponse?, error: Error?) -> Result<Data?, Error>
    func parseResponse<U: Decoding>(_ data: Data?, response: URLResponse?, error: Error?) -> Result<U,Error>
}

class HTTPResponseParser: ResponseParser {
    
    // MARK: Public
    func parseResponse(_ data: Data?, response: URLResponse?, error: Error?) -> Result<Data?, Error> {
        do {
            try parseErrors(response, error: error)
            return .success(data)
        } catch {
            return .failure(error)
        }
    }
    
    func parseResponse<U: Decoding>(_ data: Data?, response: URLResponse?, error: Error?) -> Result<U,Error> {
        do {
            try parseErrors(response, error: error)
            guard let response = try U.decode(data: data) else {
                return .failure(DecodingError.typeMismatch)
            }
            return .success(response)
        } catch {
            return .failure(error)
        }
    }
    
    // MARK: Private
    private func parseErrors(_ response: URLResponse?, error: Error?) throws {
        if let error = error {
            throw NetworkError.localError(error)
        }
        guard let httpResponse = response as? HTTPURLResponse else {
            throw NSError(domain: "ceva", code: 22, userInfo: nil)
        }
        switch httpResponse.statusCode {
        case let code where (400...499).contains(code):
            throw NetworkError.clientError(code)
        case let code where (500...599).contains(code):
            throw NetworkError.serverError(code)
        default:
            break
        }
    }
}
