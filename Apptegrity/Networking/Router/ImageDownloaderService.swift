//
//  ImageDownloader.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 10/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit
import Combine

enum ImageError: Error {
    case decode
    case notFoundInCache
}

protocol ImageDownloaderServiceProtocol {
    func download(url: URL) -> AnyPublisher<(URL, UIImage), Error>
}

final class ImageDownloaderService: ImageDownloaderServiceProtocol {
    
    // MARK: Properties
    private let router: Router
    private let cache: NSCache<NSString, UIImage>
    
    // MARK: Init
    init(router: Router, cache: NSCache<NSString, UIImage>) {
        self.router = router
        self.cache = cache
    }
    
    // MARK: Public
    func download(url: URL) -> AnyPublisher<(URL, UIImage), Error> {
        return Future<(URL, UIImage), Error> { [cache] promise in
            if let image = cache.object(forKey: url.absoluteString as NSString) {
                promise(.success((url, image)))
            } else {
                promise(.failure(ImageError.notFoundInCache))
            }
        }.catch { [router, cache] _ in
            router.execute(url).tryMap { data -> (URL, UIImage) in
                if let imageData = data, let image = UIImage(data: imageData) {
                    cache.setObject(image, forKey: url.absoluteString as NSString)
                    return (url, image)
                } else {
                    throw ImageError.decode
                }
            }
        }.eraseToAnyPublisher()
    }
}
