//
//  HTTPRequestBuilder.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine
import WebKit

protocol RequestBuilder {
    func buildRequest(_ endpoint: EndpointType) -> URLRequest
}

class HTTPRequestBuilder: RequestBuilder {
    
    // MARK: Properties
    private let credentialsStorage: CredentialsStorageProtocol
    private let locationService: LocationServiceProtocol
    private let userAgentService: UserAgentServiceProtocol
    
    private var locationDetails: LocationDetails?
    private var cancellables = Set<AnyCancellable>()
    
    // MARK: Init
    init(credentialsStorage: CredentialsStorageProtocol,
         locationService: LocationServiceProtocol,
         userAgentService: UserAgentServiceProtocol) {
        self.credentialsStorage = credentialsStorage
        self.locationService = locationService
        self.userAgentService = userAgentService
        self.locationDetails = self.locationService.currentLocation
        self.locationService.currentLocationPublisher
            .assignNoRetain(to: \.locationDetails, on: self)
            .store(in: &cancellables)
       
    }
    
    // MARK: Public
    func buildRequest(_ endpoint: EndpointType) -> URLRequest {
        var urlRequest = URLRequest(url: endpoint.baseURL.appendingPathComponent(endpoint.path), timeoutInterval: 20.0)
        urlRequest.httpMethod = endpoint.httpMethod.rawValue
        endpoint.httpTask.decorations.forEach { decoration in
            switch decoration {
            case .headers(let headers):
                addAdditionalHeaders(&urlRequest, headers: headers)
            case .json(let body):
                configure(&urlRequest, body: body)
            case .jsonParameters(let bodyParameters):
                configure(&urlRequest, bodyParameters: bodyParameters)
            case .urlParameters(let urlParameters):
                configure(&urlRequest, urlParameters: urlParameters)
            }
        }
        while userAgentService.userAgent.isEmpty {}
        addAdditionalHeaders(&urlRequest, headers: [HTTPHeaders.Keys.visitorID: credentialsStorage.visitorID])
        addAdditionalHeaders(&urlRequest, headers: [HTTPHeaders.Keys.userAgent: userAgentService.userAgent])
        if let locationHeaders = locationDetails?.headers, !locationHeaders.isEmpty {
            addAdditionalHeaders(&urlRequest, headers: locationHeaders )
        }
        if endpoint.requiresAuthentication {
            addAdditionalHeaders(&urlRequest, headers: [HTTPHeaders.Keys.authorization: "Bearer " + credentialsStorage.apiKey])
        }
        return urlRequest
    }
    
    // MARK: Private
    private func addAdditionalHeaders(_ urlRequest: inout URLRequest, headers: HTTPHeaders?) {
        headers?.forEach({
            urlRequest.setValue("\($0.value)", forHTTPHeaderField: $0.key)
        })
    }
    
    private func configure(_ urlRequest: inout URLRequest, body: Data?) {
        urlRequest.httpBody = body
    }
    
    private func configure(_ urlRequest: inout URLRequest, bodyParameters: HTTPParameters?) {
        if let bodyParams = bodyParameters {
            JSONParameterEncoder().encode(request: &urlRequest, with: bodyParams)
        }
    }
    
    private func configure(_ urlRequest: inout URLRequest, urlParameters: HTTPParameters?) {
        if let urlParams = urlParameters {
            URLParameterEncoder().encode(request: &urlRequest, with: urlParams)
        }
    }
    
}
