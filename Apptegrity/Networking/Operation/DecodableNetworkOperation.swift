//
//  NetworkOperation.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
import os.log

class DecodableNetworkOperation<T: Decoding>: ConcurentOperation<T> {
    
    private let endpoint: EndpointType
    private let requestBuilder: RequestBuilder
    private let responseParser: ResponseParser
    private let urlSession: URLSession
    
    private var task: URLSessionDataTask?
    
    private var absoluteString: String = ""
    
    init(urlSession: URLSession,
         endpoint: EndpointType,
         requestBuilder: RequestBuilder,
         responseParser:ResponseParser) {
        self.urlSession = urlSession
        self.endpoint = endpoint
        self.requestBuilder = requestBuilder
        self.responseParser = responseParser
        super.init()
    }
    
    override func main() {
        guard !isCancelled else {
            finish()
            return
        }
        
        let urlRequest = self.requestBuilder.buildRequest(endpoint)
        absoluteString = urlRequest.url?.absoluteString ?? ""
        task = urlSession.dataTask(with: urlRequest, completionHandler: { (data, response, error) in
            let result: Result<T, Error> = self.responseParser.parseResponse(data, response: response, error: error)
            self.complete(result: result)
        })
        os_log("%{PUBLIC}@ started", log: OSLog.decodableNetworkOperation, type: .info, absoluteString)
        task?.resume()
    }
    
    override func cancel() {
        task?.cancel()
        let result: Result<T, Error> = .failure(NSError(domain: NSURLErrorDomain, code: -999, userInfo: nil))
        completionHandler?(result)
        super.cancel()
    }
    
    deinit {
        os_log("%{PUBLIC}@ deallocated", log: OSLog.decodableNetworkOperation, type: .info, absoluteString)
    }
}
