//
//  ConcurrentOperation.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

class ConcurentOperation<T>: Operation {
    
    typealias OperationCompletionHandler = (_ result: Result<T, Error>) -> Void
    var completionHandler: (OperationCompletionHandler)?
    
    // MARK: - State
    
    enum State: String {
        case ready = "isReady"
        case executing = "isExecuting"
        case finished = "isFinished"
    }
    
    
    override var isReady: Bool {
        return super.isReady && state == .ready
    }
    
    override var isExecuting: Bool {
        return state == .executing
    }
    
    override var isFinished: Bool {
        return state == .finished
    }
    
    var state = State.ready {
        willSet {
            willChangeValue(forKey: newValue.rawValue)
            willChangeValue(forKey: state.rawValue)
        }
        didSet {
            didChangeValue(forKey: oldValue.rawValue)
            didChangeValue(forKey: state.rawValue)
        }
    }
    
    override func start() {
        guard !isCancelled else {
            finish()
            return
        }
        if !isExecuting {
            state = .executing
        }
        main()
    }
    
    override func cancel() {
        super.cancel()
        finish()
    }
    
    func finish() {
        if isExecuting {
            state = .finished
        }
    }
    
    func complete(result: Result<T, Error>) {
        finish()
        if !isCancelled {
            completionHandler?(result)
        }
    }
    
}
