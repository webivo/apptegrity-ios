//
//  UserAgent.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 03/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import WebKit

protocol UserAgentServiceProtocol {
    var userAgent: String { get }
}

class UserAgentService: UserAgentServiceProtocol {
    var userAgent: String { return agent.value }
    
    private let webView: WKWebView = WKWebView()
    private var agent: Atomic<String> = Atomic("")
    
    init() {
        load()
    }
    
    func load() {
        self.webView.evaluateJavaScript("navigator.userAgent") { [weak self] (value, error) in
            guard let userAgent = value as? String else { return }
            self?.agent.mutate { $0 = (userAgent.isEmpty ? "unknown" : userAgent) }
        }
    }
    
}
