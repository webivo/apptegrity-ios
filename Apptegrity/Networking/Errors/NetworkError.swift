//
//  NetworkError.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

enum NetworkError: Error {
   case localError(Error)
   case clientError(Int)
   case serverError(Int)
}
