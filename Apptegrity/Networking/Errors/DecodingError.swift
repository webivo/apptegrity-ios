//
//  DecodingError.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 13/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation

enum DecodingError: Error {
    case typeMismatch
}
