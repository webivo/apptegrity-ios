//
//  ShadowView.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 11/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

final class ShadowView: UIView {
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    private func commonInit() {
        layer.cornerRadius = 6
        layer.shadowColor = UIColor.lightGray.cgColor
        layer.shadowOffset = CGSize(width: 2, height: 2)
        layer.shadowRadius = 3
        layer.shadowOpacity = 0.6
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
    }
}
