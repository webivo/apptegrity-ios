//
//  MessageInput.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 08/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

protocol MessageInputDelegate: class {
    func messageInputDidTapSend(_ messageInput: MessageInput)
    func messageInputDidChangeContent(_ messageInput: MessageInput)
}

@IBDesignable
class MessageInput: UIView {
    
    enum State: Int {
        case none
        case loading
        case error
    }
    
    @IBOutlet private var contentView: UIView!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var actionButton: UIButton!
    @IBOutlet private weak var actionLoadingIndicator: UIActivityIndicatorView!
    
    @IBInspectable
    var placeholder: String = ""
    
    @IBInspectable
    var placeholderColor: UIColor = .lightGray {
        didSet {
            if self.textView.textColor == placeholderColor {
                self.textView.textColor = placeholderColor
            }
        }
    }
    
    @IBInspectable
    var text: String?  {
        get { self.textView.textColor == placeholderColor ? nil : self.textView.text }
        set {
            if let text = newValue, !text.isEmpty {
                apply(on: textView, text: text)
            } else {
                apply(on: textView, placeholder: placeholder)
            }
            actionButton.isEnabled = (textView.textColor != placeholderColor) && (!textView.text.isEmpty)
        }
    }
    
    @IBInspectable
    var actionDefaultImage: UIImage = Asset.paperplane.image {
        didSet { update(state: state) }
    }
    
    @IBInspectable
    var actionErrorImage: UIImage = Asset.error.image {
        didSet { update(state: state) }
    }
    
    var state: State = .none {
        didSet {
            update(state: state)
        }
    }
    
    weak var delegate: MessageInputDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    private func commonInit() {
        let bundle = Bundle(for: MessageInput.self)
        let view = bundle.loadNibNamed(MessageInput.className, owner: self, options: nil)?.first as! UIView
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: topAnchor),
            view.leadingAnchor.constraint(equalTo: leadingAnchor),
            view.trailingAnchor.constraint(equalTo: trailingAnchor),
            view.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        textView.delegate = self
        update(state: state)
    }
    
    private func update(state: MessageInput.State) {
        switch state {
        case .none:
            actionLoadingIndicator.isHidden = true
            actionButton.setImage(actionDefaultImage , for: .normal)
            actionButton.tintColor = .systemBlue
            actionButton.isHidden = false
        case .loading:
            textView.isUserInteractionEnabled = true
            actionLoadingIndicator.isHidden = false
            actionButton.isHidden = true
        case .error:
            actionLoadingIndicator.isHidden = true
            actionButton.tintColor = .systemRed
            actionButton.setImage(actionErrorImage , for: .normal)
            actionButton.isHidden = false
        }
    }
    
    @IBAction private func actionButtonTapped(_ sender: Any) {
        delegate?.messageInputDidTapSend(self)
    }
    
}

extension MessageInput: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        actionButton.isEnabled = textView.textColor != placeholderColor && !textView.text.isEmpty
        delegate?.messageInputDidChangeContent(self)
    }
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.window != nil && textView.textColor == placeholderColor {
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument,
                                                            to: textView.beginningOfDocument)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text!
        let swiftRange = Range(range, in: currentText)!
        let updatedText = currentText.replacingCharacters(in: swiftRange, with: text)
        if updatedText.isEmpty {
            apply(on: textView, placeholder: placeholder)
            textViewDidChange(textView)
            return false
        } else if textView.textColor == placeholderColor && !text.isEmpty {
            apply(on: textView, text: nil)
        }
        return true
    }
    
    private func apply(on textView: UITextView, placeholder: String) {
        textView.text = placeholder
        textView.textColor = placeholderColor
        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument,
                                                        to: textView.beginningOfDocument)
    }
    
    private func apply(on textView: UITextView, text: String?) {
        textView.text = nil
        textView.textColor = .black
    }
}

