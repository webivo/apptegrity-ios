//
//  GradientView.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 05/04/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

@IBDesignable
class GradientLayerView: UIView {
    
    override public class var layerClass: Swift.AnyClass {
        return CAGradientLayer.self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        guard let gradientLayer = self.layer as? CAGradientLayer else { return }
        gradientLayer.colors = [UIColor.white.withAlphaComponent(0).cgColor,
                                UIColor.white.cgColor]
        gradientLayer.locations = [0.0, 1.0]
    }
}
