//
//  FeatureDetailsViewController.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 08/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit
import WebKit
import Combine

class FeatureDetailsViewController: UIViewController {
    
    private let featureViewModel: FeatureListItemViewModel
    
    @IBOutlet private var contentLabel: UILabel!
    @IBOutlet private var featureImageView: UIImageView!
    @IBOutlet private var featureImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var messageInput: MessageInput!
    @IBOutlet private var messageInputBottomConstraint: NSLayoutConstraint!
    
    private let inputMessageInput = MessageInput(frame: .zero)
    
    private var cancellables = Set<AnyCancellable>()
    private var observer: NSKeyValueObservation?
    
    init(featureViewModel: FeatureListItemViewModel) {
        self.featureViewModel = featureViewModel
        let bundle = Bundle(for: FeatureDetailsViewController.self)
        super.init(nibName: FeatureDetailsViewController.className, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageInput.delegate = self
        bindViewModel()
        bindKeyboardEvents()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        featureViewModel.markAsOpened()
    }
    
    private func bindViewModel() {
        title = featureViewModel.date
        titleLabel.text = featureViewModel.title
        contentLabel.setHTMLFromString(text: featureViewModel.content)
        messageInput.placeholder = featureViewModel.placeholder
        
        if let url = featureViewModel.imageURL {
            featureViewModel.loadImage(url: url)
                .replaceError(with: { (url, Asset.errorPlaceholder.image) }())
                .receive(on: DispatchQueue.main)
                .sink(on: self, block: { (this, tuple) in
                    let (url, image) = tuple
                    guard this.featureViewModel.imageURL == url else { return }
                    this.featureImageView.image = image
                }).store(in: &cancellables)
        } else {
            featureImageViewHeightConstraint.constant = 0
        }
        
        featureViewModel.$featureFeedbackState
            .receive(on: RunLoop.main)
            .sink(on: self) { (this, state) in
                this.update(feedbackState: state)
        }.store(in: &cancellables)
    }
    
    private func bindKeyboardEvents() {
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
            .compactMap {
                KeyboardInfo($0)
        }.sink(on: self) { (this, info) in
            this.keyboardWillShow(info)
        }.store(in: &cancellables)
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
            .compactMap {
                KeyboardInfo($0)
        }.sink(on: self) { (this, info) in
            this.keyboardWillHide(info)
        }.store(in: &cancellables)
    }
    
    private func update(feedbackState: FeatureFeedbackState) {
        switch feedbackState {
        case .none:
            messageInput.state = .none
            if messageInput.text != nil {
                messageInput.text = nil
                updateLayout(animate: true)
            }
        case .sending:
            messageInput.state = .loading
        case .error:
            messageInput.state = .error
        }
    }
    
}

// MARK: - MessageInputDelegate
extension FeatureDetailsViewController: MessageInputDelegate {
    func messageInputDidTapSend(_ messageInput: MessageInput) {
        guard let text = messageInput.text, !text.isEmpty else { return }
        featureViewModel.send(feedback: text)
    }
    
    func messageInputDidChangeContent(_ messageInput: MessageInput) {
        updateLayout(animate: false)
    }
    
    private func updateLayout(animate: Bool) {
        if animate {
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        } else {
            UIView.performWithoutAnimation {
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: - Keyboard management
private extension FeatureDetailsViewController {
    
    func keyboardWillShow(_ info: KeyboardInfo) {
        view.layoutIfNeeded()
        messageInputBottomConstraint.constant = info.frameEnd.height
        UIView.animate(withDuration: info.animationDuration,
                       delay: 0.0,
                       options: UIView.AnimationOptions(rawValue: UInt(info.animationCurve.rawValue)),
                       animations: {
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func keyboardWillHide(_ info: KeyboardInfo) {
        view.layoutIfNeeded()
        messageInputBottomConstraint.constant = 0
        UIView.animate(withDuration: info.animationDuration,
                       delay: 0.0,
                       options: UIView.AnimationOptions(rawValue: UInt(info.animationCurve.rawValue)),
                       animations: {
                        self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
