// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Strings

// swiftlint:disable function_parameter_count identifier_name line_length type_body_length
internal enum L10n {
  /// We'll let you know when there will be any news.
  internal static let emptyFeaturesSubtitle = L10n.tr("Strings", "empty_features_subtitle")
  /// No updates available.
  internal static let emptyFeaturesTitle = L10n.tr("Strings", "empty_features_title")
  /// Try again
  internal static let errorRetryTitle = L10n.tr("Strings", "error_retry_title")
  /// We had a problem fetching our latest features.
  internal static let errorTitle = L10n.tr("Strings", "error_title")
  /// Send feedback
  internal static let sendFeedbackPlaceholder = L10n.tr("Strings", "send_feedback_placeholder")
}
// swiftlint:enable function_parameter_count identifier_name line_length type_body_length

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    // swiftlint:disable:next nslocalizedstring_key
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
