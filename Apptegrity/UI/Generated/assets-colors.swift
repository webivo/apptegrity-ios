// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit.NSColor
  internal typealias Color = NSColor
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIColor
  internal typealias Color = UIColor
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal struct ColorName {
  internal let rgbaValue: UInt32
  internal var color: Color { return Color(named: self) }

  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ffd200"></span>
  /// Alpha: 100% <br/> (0xffd200ff)
  internal static let announcement = ColorName(rgbaValue: 0xffd200ff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#38ef7d"></span>
  /// Alpha: 100% <br/> (0x38ef7dff)
  internal static let comingSoon = ColorName(rgbaValue: 0x38ef7dff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#6dd5ed"></span>
  /// Alpha: 100% <br/> (0x6dd5edff)
  internal static let fix = ColorName(rgbaValue: 0x6dd5edff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#41295a"></span>
  /// Alpha: 100% <br/> (0x41295aff)
  internal static let improvement = ColorName(rgbaValue: 0x41295aff)
  /// <span style="display:block;width:3em;height:2em;border:1px solid black;background:#ff4b2b"></span>
  /// Alpha: 100% <br/> (0xff4b2bff)
  internal static let new = ColorName(rgbaValue: 0xff4b2bff)
}
// swiftlint:enable identifier_name line_length type_body_length

// MARK: - Implementation Details

// swiftlint:disable operator_usage_whitespace
internal extension Color {
  convenience init(rgbaValue: UInt32) {
    let red   = CGFloat((rgbaValue >> 24) & 0xff) / 255.0
    let green = CGFloat((rgbaValue >> 16) & 0xff) / 255.0
    let blue  = CGFloat((rgbaValue >>  8) & 0xff) / 255.0
    let alpha = CGFloat((rgbaValue      ) & 0xff) / 255.0

    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
}
// swiftlint:enable operator_usage_whitespace

internal extension Color {
  convenience init(named color: ColorName) {
    self.init(rgbaValue: color.rgbaValue)
  }
}
