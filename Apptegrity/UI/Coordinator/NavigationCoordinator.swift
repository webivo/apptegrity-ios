//
//  NavigationCoordinator.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 07/03/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
}

class NavigationCoordinator: Coordinator {
    
    // MARK: - Private Properties
    private let whatsNewService: WhatsNewServicePublisherProtocol
    private let featureService: FeatureServicePublisherProtocol
    private let feedbackService: FeedbackServicePublisherProtocol
    private let imageDownloader: ImageDownloaderServiceProtocol
    private let navigationController: UINavigationController
    
    // MARK: - Public properties
    var onCancel: ((_ navigationCoordinator: NavigationCoordinator)->())?
    
    init(navigationController: UINavigationController,
         whatNewService: WhatsNewServicePublisherProtocol,
         featureService: FeatureServicePublisherProtocol,
         feedbackService: FeedbackServicePublisherProtocol,
         imageDownloader: ImageDownloaderServiceProtocol) {
        self.navigationController = navigationController
        self.whatsNewService = whatNewService
        self.featureService = featureService
        self.feedbackService = feedbackService
        self.imageDownloader = imageDownloader
    }
    
    func start() {
        let featuresViewModel = FeaturesViewModel(whatNewService: whatsNewService,
                                                  featureService: featureService,
                                                  feedbackService: feedbackService,
                                                  imageDownloader: imageDownloader)
        let featuresViewController = FeaturesViewController(featuresViewModel: featuresViewModel)
        featuresViewController.delegate = self
        featuresViewController.title = "What's new"
        featuresViewController.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationController.setViewControllers([featuresViewController], animated: false)
        featuresViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close,
                                                                                  target: self,
                                                                                  action: #selector(cancel))
    }
    
    @objc private func cancel() {
        navigationController.dismiss(animated: true, completion: nil)
        onCancel?(self)
    }
}

// MARK: - FeaturesViewControlleDelegate
extension NavigationCoordinator: FeaturesViewControlleDelegate {
    func featuresViewController(_ viewController: FeaturesViewController, didReceiveAction action: FeaturesViewControllerAction) {
        switch action {
        case .openDetails(let feature):
            let featureDetailsVM = FeatureListItemViewModel(feature: feature,
                                                            featureService: featureService,
                                                            feedbackService: feedbackService,
                                                            imageDownloader: imageDownloader)
            let featureDetailsVC = FeatureDetailsViewController(featureViewModel: featureDetailsVM)
            navigationController.pushViewController(featureDetailsVC, animated: true)
        }
    }
}
