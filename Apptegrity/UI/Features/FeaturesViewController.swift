//
//  FeaturesViewController.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 19/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit
import Combine

enum Section: CaseIterable {
    case main
}

enum FeaturesViewControllerAction {
    case openDetails(Feature)
}

protocol FeaturesViewControlleDelegate: class {
    func featuresViewController(_ viewController: FeaturesViewController,
                                didReceiveAction action: FeaturesViewControllerAction)
}

class FeaturesViewController: UIViewController {
    
    let featuresViewModel: FeaturesViewModel
    
    private lazy var emptyView: EmptyView = {
        let emptyView = EmptyView(frame: .zero, theme: EmptyFeaturesTheme())
        return emptyView
    }()
    
    private weak var loadingViewController: LoadingViewController?
    private weak var errorViewController: ErrorViewController?
    
    private lazy var dataSource: UITableViewDiffableDataSource<Section, Feature> = {
        return UITableViewDiffableDataSource<Section, Feature>(tableView: tableView) { [weak self]  (tableView: UITableView, indexPath: IndexPath, feature: Feature) -> UITableViewCell? in
            guard let self = self else { return nil }
            let cell = tableView.dequeueReusableCell(with: FeatureListItemTableViewCell.self, for: indexPath)
            cell.featureListItemViewModel = self.featuresViewModel.featureListItemViewModel(at: indexPath.row)
            return cell
        }
    }()
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.backgroundColor = .systemGroupedBackground
        tableView.showsVerticalScrollIndicator = false
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 400
        tableView.tableFooterView = UIView()
        tableView.register(cellType: FeatureListItemTableViewCell.self,
                           bundle: Bundle(for: FeatureListItemTableViewCell.self))
        return tableView
    }()
    
    private lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(reload), for: .valueChanged)
        return refreshControl
    }()
    
    private var cancellables = Set<AnyCancellable>()
    weak var delegate: FeaturesViewControlleDelegate?
    
    init(featuresViewModel: FeaturesViewModel) {
        self.featuresViewModel = featuresViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init?(coder: NSCoder) not implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        bindViewModel()
        bindKeyboardEvents()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if case .unknown = featuresViewModel.state {
            featuresViewModel.markAsSeen()
            featuresViewModel.fetch()
        }
    }
    
    private func bindKeyboardEvents() {
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
            .compactMap { KeyboardInfo($0) }
            .sink(on: self) { (featuresVC, info) in
                featuresVC.keyboardWillShow(info: info)
        }.store(in: &cancellables)
        
        NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
            .compactMap { KeyboardInfo($0) }
            .sink(on: self) { (featuresVC, info) in
                featuresVC.keyboardWillHide(info: info)
        }.store(in: &cancellables)
    }
    
    private func bindViewModel() {
        featuresViewModel.$state.receive(on: DispatchQueue.main).sink(on: self) { (featuresVC, state) in
            featuresVC.update(using: state)
        }.store(in: &cancellables)
    }
    
    private func update(using state: FeaturesViewState) {
        switch state {
        case .loading:
            let loadingVC = LoadingViewController()
            self.loadingViewController = loadingVC
            self.errorViewController?.remove()
            self.add(loadingVC)
        case .empty:
            self.refreshControl.endRefreshing()
            self.errorViewController?.remove()
            self.loadingViewController?.remove()
            self.showFeatures(features: [])
            self.tableView.backgroundView = emptyView
        case .data(let features):
            self.refreshControl.endRefreshing()
            self.errorViewController?.remove()
            self.loadingViewController?.remove()
            self.showFeatures(features: features)
        case .error:
            self.refreshControl.endRefreshing()
            self.loadingViewController?.remove()
            let errorVC = ErrorViewController(theme: FeaturesListErrorViewTheme())
            errorVC.reloadHandler = { [weak self] in
                self?.fetch()
            }
            self.add(errorVC)
            self.errorViewController = errorVC
        default:
            break
        }
    }
    
    private func showFeatures(features: [Feature]) {
        if tableView.superview == nil {
            tableView.refreshControl = refreshControl
            view.addSubview(tableView)
            NSLayoutConstraint.activate([
                tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                tableView.topAnchor.constraint(equalTo: view.topAnchor),
                tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        }
        var snapshot = NSDiffableDataSourceSnapshot<Section, Feature>()
        snapshot.appendSections([.main])
        snapshot.appendItems(features, toSection: .main)
        dataSource.apply(snapshot, animatingDifferences: true)
        tableView.backgroundView = nil
    }
}

private extension FeaturesViewController {
    
    private func fetch() {
        featuresViewModel.fetch()
    }
    
    @objc private func reload() {
        view.endEditing(true)
        featuresViewModel.refresh()
    }
}

private extension FeaturesViewController {
    func keyboardWillShow(info: KeyboardInfo) {
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: info.frameEnd.height, right: 0)
    }
    
    func keyboardWillHide(info: KeyboardInfo) {
        tableView.contentInset = UIEdgeInsets.zero
    }
}

// MARK: - UITableViewDataSource
extension FeaturesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.endEditing(true)
        guard let featureDetailsVM = featuresViewModel.featureListItemViewModel(at: indexPath.row) else {
            return
        }
        if let delegate = delegate {
            delegate.featuresViewController(self, didReceiveAction: .openDetails(featureDetailsVM.feature))
        } else {
            let featureDetailsVC = FeatureDetailsViewController(featureViewModel: featureDetailsVM)
            present(featureDetailsVC, animated: true, completion: nil)
        }
    }
    
}
