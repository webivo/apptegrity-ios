//
//  FeaturesViewModel.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 19/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

internal enum FeaturesViewState {
    case unknown
    case loading
    case error(Error)
    case empty
    case data([Feature])
}

internal class FeaturesViewModel {
    
    // MARK: - Private Properties
    private let whatsNewService: WhatsNewServicePublisherProtocol
    private let featureService: FeatureServicePublisherProtocol
    private let feedbackService: FeedbackServicePublisherProtocol
    private let imageDownloader: ImageDownloaderServiceProtocol
    
    @Published private(set) var state: FeaturesViewState = .unknown
    
    private var getFeaturesCancellable: AnyCancellable?
    
    init(whatNewService: WhatsNewServicePublisherProtocol,
         featureService: FeatureServicePublisherProtocol,
         feedbackService: FeedbackServicePublisherProtocol,
         imageDownloader: ImageDownloaderServiceProtocol) {
        self.whatsNewService = whatNewService
        self.featureService = featureService
        self.feedbackService = feedbackService
        self.imageDownloader = imageDownloader
    }
    
    func markAsSeen() {
        _ = self.whatsNewService.seen()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
    }
    
    func refresh() {
        getFeaturesCancellable = self.featureService.getFeatures()
            .receive(on: DispatchQueue.main)
            .map { return $0.isEmpty ? FeaturesViewState.empty : FeaturesViewState.data($0) }
            .catch { return Just(FeaturesViewState.error($0)) }
            .assignNoRetain(to: \.state, on: self)
    }
    
    func fetch() {
        state = .loading
        refresh()
    }
    
    func featureListItemViewModel(at index: Int) -> FeatureListItemViewModel? {
        guard case .data(let features) = state else { return nil }
        assert(features.count > index)
        let featureListViewModel = FeatureListItemViewModel(feature: features[index],
                                                            featureService: featureService,
                                                            feedbackService: feedbackService,
                                                            imageDownloader: imageDownloader)
        return featureListViewModel
    }
}
