//
//  FeatureListItemViewModel.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 19/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import UIKit
import Combine

enum FeatureFeedbackState {
    case none
    case sending
    case error
}

class FeatureListItemViewModel {
    
    let feature: Feature
    private let featureService: FeatureServicePublisherProtocol
    private let feedbackService: FeedbackServicePublisherProtocol
    private let imageDownloader: ImageDownloaderServiceProtocol
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd, yyyy"
        return formatter
    }()
    
    var title: String { return feature.title }
    var date: String { return dateFormatter.string(from: feature.date)}
    var content: String { return feature.content }
    var imageURL: URL? { return feature.imageUrl != nil ? URL(string: feature.imageUrl!) : nil }
    var featureState: String { return feature.state.value }
    var featureStateColor: UIColor { return feature.stateColor }
    var placeholder: String { return L10n.sendFeedbackPlaceholder }
    
    private var sendFeedbackCancellable: AnyCancellable?
    private var markAsOpenedCancellable: AnyCancellable?
    private var loadImageCancellable: AnyCancellable?
    
    @Published private(set) var featureFeedbackState: FeatureFeedbackState = .none
    
    init(feature: Feature,
         featureService: FeatureServicePublisherProtocol,
         feedbackService: FeedbackServicePublisherProtocol,
         imageDownloader: ImageDownloaderServiceProtocol) {
        self.feature = feature
        self.featureService = featureService
        self.feedbackService = feedbackService
        self.imageDownloader = imageDownloader
    }
    
    func send(feedback: String) {
        let feedback = Feedback(text: feedback)
        featureFeedbackState = .sending
        self.sendFeedbackCancellable = feedbackService.post(feedback: feedback, onFeature: feature.id)
            .map { _ in FeatureFeedbackState.none }
            .replaceError(with: .error)
            .assignNoRetain(to: \.featureFeedbackState, on: self)
    }
    
    func markAsOpened() {
        self.markAsOpenedCancellable = featureService.markAsOpenedFeature(id: feature.id)
            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
    }
    
    func loadImage(url: URL) -> AnyPublisher<(URL, UIImage), Error> {
        return imageDownloader.download(url: url)
    }
}
