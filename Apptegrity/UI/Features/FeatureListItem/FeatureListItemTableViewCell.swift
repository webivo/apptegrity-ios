//
//  FeatureListItemTableViewCell.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 19/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit
import Combine

class FeatureListItemTableViewCell: UITableViewCell {
    
    @IBOutlet private var typeContainerView: UIView!
    @IBOutlet private var typeLabel: UILabel!
    @IBOutlet private var contentLabel: UILabel!
    @IBOutlet private var dateLabel: UILabel!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var featureImageView: UIImageView!
    @IBOutlet private var featureImageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet private var cardView: UIView!
    @IBOutlet private var shadowView: UIView!
    
    private var originalFeatureImageViewHeight: CGFloat!
    
    var featureListItemViewModel: FeatureListItemViewModel! {
        didSet {
            bindViewModel()
        }
    }
    private var cancellables = Set<AnyCancellable>()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // FeatureImageView
        originalFeatureImageViewHeight = featureImageViewHeightConstraint.constant
        // Type Views
        typeContainerView.layer.cornerRadius = 10.0
        typeContainerView.layer.masksToBounds = true       
        //CardView
        cardView.layer.masksToBounds = true
        cardView.layer.cornerRadius = 6
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        cancellables.removeAll()
        featureImageView.image = nil
        originalFeatureImageViewHeight = featureImageViewHeightConstraint.constant
    }
    
    private func bindViewModel() {
        cancellables.removeAll()
      
        dateLabel.text = featureListItemViewModel.date
        typeLabel.text = featureListItemViewModel.featureState
        typeContainerView.backgroundColor = featureListItemViewModel.featureStateColor
        titleLabel.text = featureListItemViewModel.title
        contentLabel.setHTMLFromString(text: featureListItemViewModel.content)
    
        if let url = featureListItemViewModel.imageURL {
            featureListItemViewModel.loadImage(url: url)
                .receive(on: DispatchQueue.main)
                .replaceError(with: { (url, Asset.errorPlaceholder.image) }())
                .sink(on: self, block: { (this, tuple) in
                    let (url, image) = tuple
                    guard this.featureListItemViewModel.imageURL == url else { return }
                    this.featureImageView.image = image
                }).store(in: &cancellables)
        } else {
            featureImageViewHeightConstraint.constant = 0
        }
    }
    
}
