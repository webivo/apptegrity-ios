//
//  FeaturesListErrorViewTheme.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 24/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

struct FeaturesListErrorViewTheme: ErrorViewTheme {
    
    let icon: UIImage = Asset.sad.image
    
    let title: String = L10n.errorTitle
    let titleColor: UIColor = .darkGray
    let titleFont: UIFont = .systemFont(ofSize: 18, weight: .light)
    
    let actionTitle: String = L10n.errorRetryTitle
    let actionTitleColor: UIColor = .white
    let actionTitleBackgroundColor: UIColor = .systemBlue
    let actionTitleFont: UIFont = .systemFont(ofSize: 18)
}
