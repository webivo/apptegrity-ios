//
//  EmptyView.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 04/03/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

class EmptyView: UIView {
    
    private var theme: EmptyViewTheme = DefaultEmptyViewTheme() {
        didSet {
            updateStyle()
            setNeedsLayout()
        }
    }
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    private lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 2
        return label
    }()
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 48
        stackView.alignment = .center
        return stackView
    }()
    
    init(frame: CGRect, theme: EmptyViewTheme) {
        super.init(frame: CGRect.zero)
        self.theme = theme
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.commonInit()
    }
    
    private func commonInit() {
        addSubview(stackView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
        
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20)
        ])
        updateStyle()
    }
    
    private func updateStyle() {
        titleLabel.text = theme.title
        titleLabel.font = theme.titleFont
        titleLabel.textColor = theme.titleColor
        
        subtitleLabel.text = theme.subtitle
        subtitleLabel.font = theme.subtitleFont
        subtitleLabel.textColor = theme.subtitleColor
    }
}
