//
//  EmptyViewTheme.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 04/03/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

protocol EmptyViewTheme {
    var icon: UIImage? { get }
   
    var title: String { get }
    var titleColor: UIColor { get }
    var titleFont: UIFont { get }
    
    var subtitle: String { get }
    var subtitleColor: UIColor { get }
    var subtitleFont: UIFont { get }
}

struct DefaultEmptyViewTheme: EmptyViewTheme {
    var icon: UIImage? = nil
    
    let title: String = "Empty"
    let titleColor: UIColor = .black
    let titleFont: UIFont = .systemFont(ofSize: 30)
    
    let subtitle: String = "Subtitle"
    let subtitleColor: UIColor = .black
    let subtitleFont: UIFont = .systemFont(ofSize: 20)
}

struct EmptyFeaturesTheme:EmptyViewTheme {
    var icon: UIImage? = nil
    
    let title: String = L10n.emptyFeaturesTitle
    let titleColor: UIColor = .black
    let titleFont: UIFont = .systemFont(ofSize: 30)
    
    let subtitle: String = L10n.emptyFeaturesSubtitle
    let subtitleColor: UIColor = .darkGray
    let subtitleFont: UIFont = .systemFont(ofSize: 20)
}
