//
//  ErrorViewController.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 22/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

class ErrorViewController: UIViewController {
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .clear
        imageView.tintColor = .darkGray
        imageView.image = theme.icon
        return imageView
    }()
    
    private lazy var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 3
        label.textAlignment = .center
        label.text = theme.title
        label.font = theme.titleFont
        label.textColor = theme.titleColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var retryButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle(theme.actionTitle, for: .normal)
        button.titleLabel?.textColor = theme.actionTitleColor
        button.titleLabel?.font = theme.actionTitleFont
        button.backgroundColor = theme.actionTitleBackgroundColor
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 20)
        button.layer.cornerRadius = 4
        button.layer.masksToBounds = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    var reloadHandler: () -> Void = {}
    private let theme: ErrorViewTheme
    
    init(theme: ErrorViewTheme) {
        self.theme = theme
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        applyConstraints()
        retryButton.addTarget(self, action: #selector(retryButtonPressed(sender:)), for: .touchUpInside)
    }
    
    private func applyConstraints() {
        view.addSubview(imageView)
        view.addSubview(label)
        view.addSubview(retryButton)
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView.bottomAnchor.constraint(equalTo: view.centerYAnchor, constant: -30),
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            label.topAnchor.constraint(equalTo: view.centerYAnchor),
            label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            view.leadingAnchor.constraint(lessThanOrEqualTo: label.leadingAnchor, constant: 20),
            view.trailingAnchor.constraint(greaterThanOrEqualTo: label.trailingAnchor, constant: 20),
            retryButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            retryButton.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 40)
        ])
    }
}

private extension ErrorViewController {
    
    @objc func retryButtonPressed(sender: UIControl) {
        reloadHandler()
    }
}
