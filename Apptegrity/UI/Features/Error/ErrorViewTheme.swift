//
//  ErrorViewTheme.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 24/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

protocol ErrorViewTheme {
    var icon: UIImage { get }
    var title: String { get }
    var titleColor: UIColor { get }
    var titleFont: UIFont { get }
    
    var actionTitle: String { get }
    var actionTitleColor: UIColor { get }
    var actionTitleBackgroundColor: UIColor { get }
    var actionTitleFont: UIFont { get }
}

