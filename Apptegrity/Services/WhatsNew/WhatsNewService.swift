//
//  WhatsNewService.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 12/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

protocol WhatsNewServiceProtocol {
    func seen(completion: @escaping ((Result<Empty, Error>) -> Void))
}
protocol WhatsNewServicePublisherProtocol {
    func seen() -> AnyPublisher<Empty, Error>
}

final class WhatsNewService: WhatsNewServiceProtocol {
    
    private let router: Router
    
    init(router: Router) {
        self.router = router
    }
    
    func seen(completion: @escaping ((Result<Empty, Error>) -> Void)) {
        router.execute(WhatsNewApi.seen) { completion($0) }
    }

}

extension WhatsNewService: WhatsNewServicePublisherProtocol {
    
    func seen() -> AnyPublisher<Empty, Error> {
      return router.execute(WhatsNewApi.seen).eraseToAnyPublisher()
    }
    
}
