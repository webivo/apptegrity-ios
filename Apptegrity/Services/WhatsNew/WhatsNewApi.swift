//
//  WhatsNewApi.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 12/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation

enum WhatsNewApi: EndpointType {
    case seen
}

extension WhatsNewApi {
    var path: String { "/whatsnew/seen" }
    
    var httpMethod: HTTPMethod { .POST }
    
    var httpTask: HTTPTask {
        return HTTPTask(decorations: [
            .headers([HTTPHeaders.Keys.contentType: HTTPHeaders.Values.applicationJSON])
        ])
    }
}
