//
//  FeaturesApi.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

enum FeaturesApi: EndpointType {
    case getFeatures
    case openFeature(String)
    case seenFeature(String)
}

extension FeaturesApi {
    
    var path: String {
        switch self {
        case .getFeatures:
            return "/features"
        case .openFeature(let featureID):
            return "/features/\(featureID)/opened"
        case .seenFeature(let featureID):
            return "/features/\(featureID)/seen"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getFeatures:
            return .GET
        case .openFeature:
            return .PUT
        case .seenFeature:
            return .PUT
        }
    }
    
    var httpTask: HTTPTask {
        return HTTPTask(decorations: [
            .headers([HTTPHeaders.Keys.contentType: HTTPHeaders.Values.applicationJSON])
        ])
    }
}
