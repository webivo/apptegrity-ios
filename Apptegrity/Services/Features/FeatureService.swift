//
//  FeatureService.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import UIKit
import Combine

protocol FeatureServiceProtocol {
    func getFeatures(completion: @escaping ((Result<[Feature],Error>) -> Void))
    func markAsOpenedFeature(id: String, completion: @escaping ((Result<Empty, Error>) -> Void))
    func markAsSeenFeature(id: String, completion: @escaping ((Result<Empty, Error>) -> Void))
}

protocol FeatureServicePublisherProtocol {
    func getFeatures() -> AnyPublisher<[Feature],Error>
    func markAsOpenedFeature(id: String) -> AnyPublisher<Empty, Error>
    func markAsSeenFeature(id: String) -> AnyPublisher<Empty, Error>
}

class FeatureService: FeatureServiceProtocol {
    
    private let router: Router
    
    init(router: Router) {
        self.router = router
    }
    
    func getFeatures(completion: @escaping ((Result<[Feature], Error>) -> Void)) {
        router.execute(FeaturesApi.getFeatures) { (result: Result<[Feature], Error>) in
            completion(result)
        }
    }
    
    func markAsOpenedFeature(id: String, completion: @escaping ((Result<Empty, Error>) -> Void)) {
        router.execute(FeaturesApi.openFeature(id)) { completion($0) }
    }
    
    func markAsSeenFeature(id: String, completion: @escaping ((Result<Empty, Error>) -> Void)) {
        router.execute(FeaturesApi.seenFeature(id)) { completion($0) }
    }
}

extension FeatureService: FeatureServicePublisherProtocol {
    func getFeatures() -> AnyPublisher<[Feature], Error> {
        return router.execute(FeaturesApi.getFeatures)
    }
    
    func markAsSeenFeature(id: String) -> AnyPublisher<Empty, Error> {
        return router.execute(FeaturesApi.seenFeature(id)).eraseToAnyPublisher()
    }
    
    func markAsOpenedFeature(id: String) -> AnyPublisher<Empty, Error> {
        return router.execute(FeaturesApi.openFeature(id)).eraseToAnyPublisher()
    }
}
