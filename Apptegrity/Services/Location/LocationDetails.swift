//
//  LocationDetails.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 01/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation

struct LocationDetails {
    let country: String?
    let countryCode: String?
    let city: String?
}

extension LocationDetails {
    var headers: [String: String] {
        var dict = [String: String]()
        if let country = country { dict["X-Country"] = country }
        if let countryCode = countryCode { dict["X-Country-Code"] = countryCode }
        if let city = city { dict["X-City"] = city }
        return dict
    }
}
