//
//  LocaleLocationProvider.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 01/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

class LocaleLocationProvider: LocationProvider {
    
    lazy var currentLocation: AnyPublisher<LocationDetails?, Never> = {
        return $currentLocationDetails.eraseToAnyPublisher()
    }()
    
    @Published private(set) var currentLocationDetails: LocationDetails?
    
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        currentLocationDetails = LocationDetails(country: nil, countryCode: Locale.current.regionCode, city: nil)
        NotificationCenter.default
            .publisher(for: NSLocale.currentLocaleDidChangeNotification)
            .map { _ in
                LocationDetails(country: nil, countryCode: Locale.current.regionCode, city: nil)
            }.assignNoRetain(to: \.currentLocationDetails, on: self)
            .store(in: &cancellables)
    }
}
