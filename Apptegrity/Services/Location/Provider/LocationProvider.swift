//
//  LocationProvider.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 01/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

protocol LocationProvider {
    var currentLocation: AnyPublisher<LocationDetails?, Never> { get }
}




