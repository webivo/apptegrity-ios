//
//  CLLocationProvider.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 01/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import MapKit
import Combine
import CoreLocation

class CLLocationProvider: NSObject, LocationProvider {
    
    lazy var currentLocation: AnyPublisher<LocationDetails?, Never> = {
        return $currentLocationDetails.eraseToAnyPublisher()
    }()
    
    @Published private var currentLocationDetails: LocationDetails?
    
    private let locationManager: CLLocationManager
    private let geocoder: CLGeocoder
    
    override init() {
        self.locationManager = CLLocationManager()
        self.geocoder = CLGeocoder()
        super.init()
        self.parse(location: locationManager.location)
        self.locationManager.delegate = self
    }
}

extension CLLocationProvider: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        parse(location: locations.first)
    }
    
    private func parse(location: CLLocation?) {
        guard let location = location else { return }
        geocoder.reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
            guard let self = self else { return }
            if let placemark = placemarks?.first {
                self.currentLocationDetails = LocationDetails(country: placemark.country,
                                                              countryCode: placemark.isoCountryCode,
                                                              city: placemark.locality)
            }
        }
    }
}

