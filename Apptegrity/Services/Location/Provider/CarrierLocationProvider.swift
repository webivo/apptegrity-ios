//
//  CarrierLocationProvider.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 01/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import CoreTelephony
import Combine

class CarrierLocationProvider: NSObject, LocationProvider {
    
    lazy var currentLocation: AnyPublisher<LocationDetails?, Never> = {
        return $currentLocationDetails.eraseToAnyPublisher()
    }()
    
    @Published private var currentLocationDetails: LocationDetails?
    
    let networkStatus: CTTelephonyNetworkInfo
    
    override init() {
        self.networkStatus = CTTelephonyNetworkInfo()
        super.init()
        self.updateCurrentLocationDetails(identifier: nil)
        self.networkStatus.delegate = self
    }
}

extension CarrierLocationProvider: CTTelephonyNetworkInfoDelegate {
    
    func dataServiceIdentifierDidChange(_ identifier: String) {
        updateCurrentLocationDetails(identifier: identifier)
    }
    
    private func updateCurrentLocationDetails(identifier: String?) {
        if let identifier = identifier {
            if let carrier =  networkStatus.serviceSubscriberCellularProviders?[identifier] {
                self.currentLocationDetails = LocationDetails(country: nil, countryCode: carrier.isoCountryCode, city: nil)
            }
        } else {
            if let carrier = networkStatus.serviceSubscriberCellularProviders?.first?.value {
                self.currentLocationDetails = LocationDetails(country: nil, countryCode: carrier.isoCountryCode, city: nil)
            }
        }
    }
}
