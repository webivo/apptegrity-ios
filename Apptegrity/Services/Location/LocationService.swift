//
//  LocationService.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 01/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

protocol LocationServiceProtocol {
    var currentLocation: LocationDetails? { get }
    var currentLocationPublisher: AnyPublisher<LocationDetails?, Never> { get }
}

class LocationService: LocationServiceProtocol {
    
    var currentLocation: LocationDetails? = nil
    var currentLocationPublisher: AnyPublisher<LocationDetails?, Never>
    
    private var cancellables = Set<AnyCancellable>()
    
    let carrierProvider: CarrierLocationProvider
    let localeProvider: LocaleLocationProvider
    let cllProvider: CLLocationProvider
    
    init(carrierProvider: CarrierLocationProvider, localeProvider: LocaleLocationProvider, cllProvider: CLLocationProvider) {
        self.carrierProvider = carrierProvider
        self.localeProvider = localeProvider
        self.cllProvider = cllProvider
        self.currentLocationPublisher = cllProvider.currentLocation.flatMap { value -> AnyPublisher<LocationDetails?, Never> in
            if let locationDetails = value {
                return Just(locationDetails).eraseToAnyPublisher()
            } else {
                return carrierProvider.currentLocation
            }
        }.flatMap {
            value -> AnyPublisher<LocationDetails?, Never> in
            if let locationDetails = value {
                return Just(locationDetails).eraseToAnyPublisher()
            } else {
                return localeProvider.currentLocation
            }
        }.eraseToAnyPublisher()
        
        currentLocationPublisher
            .assignNoRetain(to: \.currentLocation, on: self)
            .store(in: &cancellables)
    }
}
