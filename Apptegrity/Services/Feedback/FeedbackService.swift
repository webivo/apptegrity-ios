//
//  FeedbackService.swift
//  Weback
//
//  Created by Claudiu Iordache on 02/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

protocol FeedbackServiceProtocol {
    func post(feedback: Feedback, onFeature featureID: String, completion: @escaping ((Result<Empty, Error>) -> Void))
}

protocol FeedbackServicePublisherProtocol {
    func post(feedback: Feedback, onFeature featureID: String) -> AnyPublisher<Empty, Error>
}

class FeedbackService: FeedbackServiceProtocol {
    
    private let router: Router
    
    init(router: Router) {
        self.router = router
    }
    
    func post(feedback: Feedback, onFeature featureID: String, completion: @escaping ((Result<Empty, Error>) -> Void)) {
        return router.execute(FeedbackApi.send(featureID, feedback), completion: completion)
    }
}

extension FeedbackService: FeedbackServicePublisherProtocol {
    func post(feedback: Feedback, onFeature featureID: String) -> AnyPublisher<Empty, Error> {
        return router.execute(FeedbackApi.send(featureID, feedback)).eraseToAnyPublisher()
    }
}
