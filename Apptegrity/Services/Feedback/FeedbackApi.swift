//
//  FeedbackApi.swift
//  Weback
//
//  Created by Claudiu Iordache on 02/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

enum FeedbackApi: EndpointType {
    case send(String, Feedback)
    
    var path: String {
        switch self {
        case .send(let featureID, _):
            return "/features/\(featureID)/feedback"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .send:
            return .POST
        }
    }
    
    var httpTask: HTTPTask {
        switch self {
        case .send(_, let feedback):
            return HTTPTask(decorations: [
                .headers([HTTPHeaders.Keys.contentType: HTTPHeaders.Values.applicationJSON]),
                .json(body: try! feedback.data())
            ])
        }
    }
    
}
