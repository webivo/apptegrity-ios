//
//  Publisher+Extension.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 28/01/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import Combine

extension Publisher where Self.Failure == Never {
    public func assignNoRetain<Root>(to keyPath: ReferenceWritableKeyPath<Root, Self.Output>, on object: Root) -> AnyCancellable where Root: AnyObject {
        sink { [weak object] (value) in
            guard let object = object else { return }
            _ = Just(value).assign(to: keyPath, on: object)
        }
    }
    
    public func sink<Root>(on object: Root, block: @escaping ((Root, Self.Output) -> Void)) -> AnyCancellable where Root: AnyObject {
        sink { [weak object] value in
            guard let object = object else { return }
            block(object, value)
        }
    }
    
}
