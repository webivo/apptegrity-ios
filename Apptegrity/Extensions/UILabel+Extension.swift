//
//  UILabel+Extension.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 05/04/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import UIKit

extension UILabel {
    func setHTMLFromString(text: String) {
        let fontName = self.font.fontName
        let fontPointSize = self.font.pointSize
        let modifiedFont = NSString(format:"<span style=\"font-family: \(fontName); font-size: \(fontPointSize)\">%@</span>" as NSString, text)
        
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] =
            [.documentType: NSAttributedString.DocumentType.html,
             .characterEncoding: String.Encoding.utf8.rawValue]
        
        guard let data = modifiedFont.data(using: String.Encoding.unicode.rawValue,
                                           allowLossyConversion: true),
            let attrStr = try? NSAttributedString(data: data, options: options, documentAttributes: nil) else {
                self.text = text
                return
        }
        self.attributedText = attrStr
    }
}
