//
//  OSLog+Extension.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 25/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation
import os.log

extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!
    static let apptegrity = OSLog(subsystem: subsystem, category: "Apptegrity")
    static let decodableNetworkOperation = OSLog(subsystem: subsystem, category: "Networking")
    static let jsonParameterEncoder = OSLog(subsystem: subsystem, category: "JSONParameterEncoder")
}
