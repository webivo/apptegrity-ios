//
//  Atomic.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 29/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import Foundation

public struct Atomic<Value> {
    private let mutex = DispatchQueue(label: "io.apptegrity.Atomic", attributes: .concurrent)
    private var _value: Value
 
    public init(_ value: Value) {
        self._value = value
    }
 
    public var value: Value { mutex.sync { _value } }
 
    public mutating func mutate<T>(execute task: (inout Value) throws -> T) rethrows -> T {
        try mutex.sync(flags: .barrier) { try task(&_value) }
    }
}
