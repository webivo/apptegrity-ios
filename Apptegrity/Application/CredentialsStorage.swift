//
//  CredentialsStorage.swift
//  Weback
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation

protocol CredentialsStorageProtocol {
    var apiKey: String { get set }
    var visitorID: String { get }
}

class CredentialsStorage: CredentialsStorageProtocol {
    var apiKey: String = ""
    
    lazy var visitorID: String = {
        guard let visitorID = UserDefaults.standard.string(forKey: "VisitorID") else {
            let generatedVisitorID = UUID().uuidString
            UserDefaults.standard.set(generatedVisitorID, forKey: "VisitorID")
            return generatedVisitorID
        }
        return visitorID
        
    }()
}
