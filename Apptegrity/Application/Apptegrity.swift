//
//  Apptegrity.swift
//  Apptegrity
//
//  Created by Claudiu Iordache on 01/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
import UIKit
import os.log

/// Entry point between your app and Apptegrity platform
public class Apptegrity {
    
    private init() {}
    private static var sharedInstance = Apptegrity()
    
    private lazy var credentialStorage: CredentialsStorageProtocol = {
        return CredentialsStorage()
    }()
    
    private lazy var userAgentService: UserAgentServiceProtocol = {
        return UserAgentService()
    }()
    
    private lazy var router: Router = {
        return Router(credentialsStorage: self.credentialStorage,
                      locationService: self.locationService,
                      userAgentService: self.userAgentService)
    }()
    
    private lazy var imageDownloader: ImageDownloaderServiceProtocol = {
        return ImageDownloaderService(router: self.router, cache: NSCache<NSString, UIImage>())
    }()
    
    private lazy var whatsNewService: WhatsNewServiceProtocol & WhatsNewServicePublisherProtocol = {
        return WhatsNewService(router: self.router)
    }()
    
    private lazy var featureService: FeatureServiceProtocol & FeatureServicePublisherProtocol = {
        return FeatureService(router: self.router)
    }()
    
    private lazy var feedbackService: FeedbackServiceProtocol & FeedbackServicePublisherProtocol = {
        return FeedbackService(router: self.router)
    }()
    
    private lazy var locationService: LocationService = {
        return LocationService(carrierProvider: CarrierLocationProvider(),
                               localeProvider: LocaleLocationProvider(),
                               cllProvider: CLLocationProvider())
    }()
    
    private var navigationCoordinator: NavigationCoordinator?
    
    /// Initialize SDK with an apiKey
    /// - Parameter apiKey: apiKey found on Apptegrity platform
    public class func initialize(with apiKey: String) {
        if apiKey.isEmpty {
            os_log("Api key is empty.", log: OSLog.apptegrity, type: .error)
        }
        Apptegrity.sharedInstance.credentialStorage.apiKey = apiKey
    }
    
    /// Built-in screen that displays all your latest news from Apptegrity platform.
    /// - Important: Call `Apptegrity.initialize(with:)` before this method
    private class func makeViewController() -> UIViewController {
        if sharedInstance.credentialStorage.apiKey.isEmpty {
            os_log("Initializing screen with empty key.", log: OSLog.apptegrity, type: .error)
        }
        let featuresViewModel = FeaturesViewModel(whatNewService: sharedInstance.whatsNewService,
                                                  featureService: sharedInstance.featureService,
                                                  feedbackService: sharedInstance.feedbackService,
                                                  imageDownloader: sharedInstance.imageDownloader)
        let featuresViewController = FeaturesViewController(featuresViewModel: featuresViewModel)
        featuresViewController.title = "What's new"
        return featuresViewController
    }
    
    /// Utility method to present the built-in screen inside a navigation stack.
    /// - Parameter viewController: presenting view controller
    /// - Important: Call `Apptegrity.initialize(with:)` before this method
    public class func present(from viewController: UIViewController) {
        if sharedInstance.credentialStorage.apiKey.isEmpty {
            os_log("Initializing screen with empty key.", log: OSLog.apptegrity, type: .error)
        }
        let navigationController = UINavigationController()
        navigationController.modalPresentationStyle = .fullScreen
        sharedInstance.navigationCoordinator = NavigationCoordinator(navigationController: navigationController,
                                                                     whatNewService: sharedInstance.whatsNewService,
                                                                     featureService: sharedInstance.featureService,
                                                                     feedbackService: sharedInstance.feedbackService,
                                                                     imageDownloader: sharedInstance.imageDownloader)
        sharedInstance.navigationCoordinator?.start()
        sharedInstance.navigationCoordinator?.onCancel = { [sharedInstance] _ in
            sharedInstance.navigationCoordinator = nil
        }
        viewController.present(navigationController, animated: true, completion: nil)
    }
    
}
