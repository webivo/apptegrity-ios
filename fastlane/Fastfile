
default_platform(:ios)

before_all do |lane, options|
  # ...
end

platform :ios do

  private_lane :archive_carthage do |options|
    version = options[:version]
    carthage(command: 'build',       
        no_skip_current: true,     
        platform: 'iOS', 
        configuration: 'Release',
        cache_builds: true)
    carthage(frameworks: ['Apptegrity'],  
            output: 'build/artifacts/' + version +'-Apptegrity.framework.zip',
            command: 'archive')
  end

  private_lane :archive_cocoapods do |options|
    gym(scheme: "ApptegrityFat", 
        configuration: "Release",
        skip_archive: true)
  end

  lane :deploy_tag do 
    increment_build_number
    version = get_version_number(xcodeproj: "Apptegrity.xcodeproj", target: "Apptegrity")
    build_number = get_build_number(xcodeproj: "Apptegrity.xcodeproj")
    full_version = version + "." + build_number
    add_git_tag(tag: "v-" + version + "." + build_number)
  end
end
