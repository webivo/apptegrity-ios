//
//  LocationDetailsTests.swift
//  ApptegrityTests
//
//  Created by Claudiu Iordache on 02/02/2020.
//  Copyright © 2020 Claudiu Iordache. All rights reserved.
//

import XCTest
@testable import Apptegrity

class LocationDetailsTests: XCTestCase {

    func testEmptyLocationDetails() {
        let locationDetails = LocationDetails(country: nil, countryCode: nil, city: nil)
        XCTAssertTrue(locationDetails.headers.isEmpty)
    }
    
    func testPartialLocationDetails() {
        let locationDetails = LocationDetails(country: "Romania", countryCode: nil, city: nil)
        XCTAssertEqual(locationDetails.headers, ["X-Country": "Romania"])
    }
    
    func testFullLocationDetails() {
        let locationDetails = LocationDetails(country: "Romania", countryCode: "RO", city: "Bucharest")
        XCTAssertEqual(locationDetails.headers, ["X-Country": "Romania", "X-Country-Code": "RO", "X-City": "Bucharest"])
    }
   
}
