//
//  MockGetEndpoint.swift
//  WebackTests
//
//  Created by Claudiu Iordache on 02/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import Foundation
@testable import Apptegrity

struct MockEndpoint: EndpointType {
    
    var method: HTTPMethod = .GET
    var task = HTTPTask.empty()
    
    var path: String {
        return "/mock"
    }
    
    var httpMethod: HTTPMethod {
        return method
    }
    
    var httpTask: HTTPTask {
        return task
    }
    
}
