//
//  HTTPRequestBuilderTests.swift
//  WebackTests
//
//  Created by Claudiu Iordache on 02/12/2019.
//  Copyright © 2019 Claudiu Iordache. All rights reserved.
//

import XCTest
import Combine
@testable import Apptegrity

class MockCredentialsStore: CredentialsStorageProtocol {
    var apiKey: String = "apiKey"
    var visitorID: String = "visitorID"
}

class MockLocationService: LocationServiceProtocol {
    var currentLocation: LocationDetails? = LocationDetails(country: "Romania", countryCode: "RO", city: "Iasi")
    lazy var currentLocationPublisher: AnyPublisher<LocationDetails?, Never> = {
        return Just<LocationDetails?>(currentLocation).eraseToAnyPublisher()
    }()
}

class MockUserAgentService: UserAgentServiceProtocol {
    var userAgent: String = "user-agent"
}

class HTTPRequestBuilderTests: XCTestCase {
    
    private var urlRequest: URLRequest!
    private var credentialsStore: MockCredentialsStore!
    private var locationService: MockLocationService!
    private var userAgentService: MockUserAgentService!
    private var builder: HTTPRequestBuilder!
    
    override func setUp() {
        credentialsStore = MockCredentialsStore()
        locationService = MockLocationService()
        userAgentService = MockUserAgentService()
        builder = HTTPRequestBuilder(credentialsStorage: credentialsStore,
                                     locationService: locationService,
                                     userAgentService: userAgentService)
        urlRequest = URLRequest(url: URL(string: "www.dummy.com")!)
    }
    
    func testGetEndpointURLMethod() {
        var endpoint = MockEndpoint()
        endpoint.method = .GET
        let request = builder.buildRequest(endpoint)
        XCTAssert(request.httpMethod == HTTPMethod.GET.rawValue)
    }
    
    func testPostEndpointURLMethod() {
        var endpoint = MockEndpoint()
        endpoint.method = .POST
        let request = builder.buildRequest(endpoint)
        XCTAssert(request.httpMethod == HTTPMethod.POST.rawValue)
    }
    
    func testPutEndpointURLMethod() {
        var endpoint = MockEndpoint()
        endpoint.method = .PUT
        let request = builder.buildRequest(endpoint)
        XCTAssert(request.httpMethod == HTTPMethod.PUT.rawValue)
    }
    
    func testDefaultURLRequest() {
        let request = builder.buildRequest(MockEndpoint())
        XCTAssert(request.httpBody == nil)
        XCTAssert(request.allHTTPHeaderFields?.keys.contains("VisitorID") ?? false)
        XCTAssert(request.allHTTPHeaderFields?.keys.contains("Authorization") ?? false)
        XCTAssert(request.allHTTPHeaderFields?.contains(where: { tuple -> Bool in
            let (key, value) = tuple
            return key == "User-Agent" && value == userAgentService.userAgent
        }) ?? false)
        locationService.currentLocation!.headers.forEach {
            XCTAssert(request.allHTTPHeaderFields?.keys.contains($0.key) ?? false)
        }
    }
    
    func testAdditionalHeadersAreAddedToURLRequest() {
        var endpoint = MockEndpoint()
        endpoint.task = HTTPTask(decorations: [.headers( ["key": "value"])])
        let request = builder.buildRequest(endpoint)
        XCTAssert(request.allHTTPHeaderFields?.keys.contains("key") ?? false)
    }
    
    func testJSONDataIsAddedToURLRequestBody() {
        var endpoint = MockEndpoint()
        endpoint.task = HTTPTask(decorations: [.jsonParameters(["key": "value"])])
        let request = builder.buildRequest(endpoint)
        XCTAssertNotNil(request.httpBody)
        let dict = try! JSONSerialization.jsonObject(with: request.httpBody!, options: []) as! [String: String]
        XCTAssert(dict == ["key":"value"])
    }
    
    // TODO : test data json
    // TODO : test url parameters
}
