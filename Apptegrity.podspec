#
#  Be sure to run `pod spec lint Apptegrity.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  spec.name         = "Apptegrity"
  spec.version      = "1.0.1"
  spec.summary      = "Keep your users engaged with Apptegrity platform."

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = "Apptegrity if a framework for seeing What's new on Apptegrity.io."
  spec.homepage     = "https://bitbucket.org/webivo/apptegrity-ios/src/master/README.md"

  # ―――  Spec License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  spec.license      = { :type => "MIT", :file => "LICENSE" }

  # ――― Author Metadata  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― # 
  spec.author             = "Claudiu Iordache"

  # ――― Platform Specifics ――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  spec.platform     = :ios
  spec.platform     = :ios, "13.0"
  spec.swift_version = '4.2'


  # ――― Source Location ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  spec.source   = { :git => "https://IordacheClaudiu@bitbucket.org/webivo/apptegrity-ios.git", :tag => spec.version }
  spec.source_files = "Apptegrity/**/*.{h,m,swift}"
  spec.ios.resources = "Apptegrity/Resources/*.{json,xcassets,strings,xib}"

end
